# New Partner A CI Template

## Purpose of the CI template

This is a hello world example 

## Overview of the jobs / workflow

This area would include a pipeline diagram and more technical details of the jobs and stages! 

## How to use this CI pipeline 

To import this template in your GitLab CI pipeline, enter in the following `include` snippet of code in your `.gitlab-ci.yml` file: 

<Remeber to update `<partner-project-slug>` to your project's slug and enter your name within `/.<partner>-gitlab-ci.yml`.>

```yaml
include:
  - project: 'partner-ci-templates/<partner-project-slug>'
    ref: main
    file: '/.<partner>-gitlab-ci.yml'
```

Subsequently, define the stages in the parent CI yaml that you desire to include from the CI template into the main pipeline definition. 

```yaml
stages:
  - parent # This is a stage pre-defined in the desired pipeline
  - echo_1 # This is a stage that is imported from the CI template from the `include` code block
  - echo_2 # This is a stage that is imported from the CI template from the `include` code block
```

## Example pipeline 

<Link to any examples or screenshots with demo code showcasing a successfully run GitLab CI pipeline>
